<?php

declare(strict_types=1);

namespace App\Task1;

class FightArena
{
    private $fighters = [];
    
    public function add(Fighter $fighter): void
    {
        array_push($this->fighters, $fighter);
    }

    public function mostPowerful(): Fighter
    {
        $maxAttackFighter = $this->fighters[0];
        foreach($this->fighters as $fighter) {
            $attack = $fighter->getAttack();
            if ($attack > $maxAttackFighter->getAttack()) {
                $maxAttackFighter = $fighter;
            }
        }
        return $maxAttackFighter;
    }

    public function mostHealthy(): Fighter
    {
        $healthier = $this->fighters[0];
        for ($i = 0; $i <= count($this->fighters); $i++) {
            if ($healthier->getHealth() > $this->fighters[$i]->getHealth()) {
                return $healthier;
            }
            $healthier = $this->fighters[$i];
        }
        return $healthier;
    }

    public function all(): array
    {
        return $this->fighters;
    }
}

<?php

declare(strict_types=1);

namespace App\Task3;

use App\Task1\FightArena;

class FightArenaHtmlPresenter
{
    public function present(FightArena $arena): string
    {
        // @todo
        $fighters = $arena->all();
        $output = '';
        foreach ($fighters as $fighter) {
            $output .= "<li>{$fighter->getName()}: {$fighter->getHealth()}, {$fighter->getAttack()}";
            $output .= "<img src=\"{$fighter->getImage()}\"></li>";
        }
        return $output;
    }
}

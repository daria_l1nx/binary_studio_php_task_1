<?php

declare(strict_types=1);

namespace App\Task2;

class EmojiGenerator
{
    private $emojiArr = ['🚀', '🚃', '🚄', '🚅', '🚇'];

    public function generate(): \Generator
    {
        foreach ($this->emojiArr as $emoji) {
            yield $emoji;
        }
    }
}
